import 'dart:io';

import 'package:agenda_contatos/main.dart';
import 'package:flutter/material.dart';
import 'package:agenda_contatos/helpers/contatos_helper.dart';
import 'dart:async';
import 'package:image_picker/image_picker.dart';

class ContatoPage extends StatefulWidget {
  final Contato contato;
  ContatoPage({this.contato});

  @override
  _ContatoPageState createState() => _ContatoPageState();
}

class _ContatoPageState extends State<ContatoPage> {
  Contato editarContato;
  bool usuarioEditou = false;
  final nomeControle = TextEditingController();
  final emailControle = TextEditingController();
  final telefoneControle = TextEditingController();
  final nomeFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    if (widget.contato == null) {
      editarContato = Contato();
    } else {
      // editarContato = widget.contato;
      editarContato =
          Contato.deMapaParaContato(widget.contato.deContatoParaMapa());
      nomeControle.text = editarContato.nome;
      emailControle.text = editarContato.email;
      telefoneControle.text = editarContato.telefone;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: dialogoSair,
      child: Scaffold(
        appBar: AppBar(
          title: Text(editarContato.nome ?? "Novo Contato"),
          centerTitle: true,
        ),
        backgroundColor: colorBackground,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (editarContato.nome != null && editarContato.nome.isNotEmpty) {
              //nome não for null nem estiver vazio
              Navigator.pop(context, editarContato);
            } else {
              FocusScope.of(context).requestFocus(nomeFocus);
            }
          },
          child: Icon(Icons.save),
          backgroundColor: colorMain,
        ),
        body: body(),
      ),
    );
  }

  Widget body() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(10),
      child: Column(
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              ImagePicker.pickImage(source: ImageSource.gallery).then(
                (arquivo) {
                  if (arquivo == null)
                    return;
                  else {
                    usuarioEditou = true;
                    setState(() {
                      editarContato.foto = arquivo.path;
                    });
                  }
                },
              );
            },
            child: Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                // color: Colors.green,
                image: DecorationImage(
                  image: editarContato.foto != null
                      ? FileImage(File(editarContato.foto))
                      : AssetImage("images/pessoa.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          textField("Nome:", nomeControle, null, (texto) {
            usuarioEditou = true;
            setState(() {
              editarContato.nome = texto;
            });
          }, focusNode: nomeFocus),
          textField("Email:", emailControle, TextInputType.emailAddress,
              (texto) {
            usuarioEditou = true;
            editarContato.email = texto;
          }),
          textField("Telefone:", telefoneControle, TextInputType.phone,
              (texto) {
            usuarioEditou = true;
            editarContato.telefone = texto;
          }),
        ],
      ),
    );
  } //fim body

  Widget textField(String labelText, TextEditingController controle,
      TextInputType textInputType, void onChaged(texto),
      {FocusNode focusNode}) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: TextField(
        keyboardType: textInputType,
        controller: controle,
        focusNode: focusNode,
        style: TextStyle(
          color: colorMain,
        ),
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          labelText: labelText,
          labelStyle: TextStyle(
            color: colorMain,
            fontWeight: FontWeight.bold,
          ),
        ),
        onChanged: onChaged,
      ),
    );
  }

  Future<bool> dialogoSair() {
    if (usuarioEditou) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(
              "Deseja sair sem salvar?",
              style: TextStyle(color: colorMain),
            ),
            content: Text("Se você sair as alterações serão perdidas"),
            actions: <Widget>[
              FlatButton(
                  child: Text("Cancelar"),
                  color: colorMain,
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              FlatButton(
                child: Text("Sair"),
                color: colorMain,
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  }
} //fim da class
